import {BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import Home from "./components/Home";
import About from "./components/About";
import "bootstrap/scss/bootstrap.scss";
import './App.scss';

function App() {
    return (
        <Router>
            <header>
                <nav className="navbar fixed-top navbar-expand-md navbar-light bg-light">
                    <div className="container">
                        <Link className="navbar-brand" to="/" title="Qwest Tv Logo Neutral">Qwest Tv Logo</Link>
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <section className="collapse navbar-collapse d-md-flex justify-content-between"
                                 id="navbarSupportedContent">
                            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                <li className="nav-item">
                                    <Link className="nav-link active" aria-current="page" to="/">Home</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="/about">About</Link>
                                </li>
                            </ul>

                            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                <li className="nav-item dropdown">
                                    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                       data-bs-toggle="dropdown" aria-expanded="false">
                                        Qwest On Your TV
                                    </a>
                                    <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <li><a className="dropdown-item" title="QwestTV Jazz" href="/jazz/">Jazz</a>
                                        </li>
                                        <li><a className="dropdown-item" title="QwestTV Mix" href="/mix/">Mix</a></li>
                                        <li><a className="dropdown-item" title="QwestTV Classic"
                                               href="/classic/">Classic</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a className="nav-link" href="/svod/" title="QwestTV SVOD">SVOD</a>
                                </li>
                            </ul>

                            <ul className="navbar-nav me-auto mb-2 mb-lg-0 align-items-baseline">
                                <li className="nav-item dropdown">
                                    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                       data-bs-toggle="dropdown" aria-expanded="false">
                                        You
                                    </a>
                                    <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <li><a className="dropdown-item" title="QwestTV Jazz"
                                               href="/profile/">Profile</a></li>
                                        <li><a className="dropdown-item" title="QwestTV Mix"
                                               href="/settings/">Settings</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a className="btn btn-sm btn-primary" href="/svod/"
                                       title="QwestTV SVOD">Disconnect</a>
                                </li>
                            </ul>

                        </section>
                    </div>
                </nav>
            </header>
            <main>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/about" component={About} />
                    <Route path="/svod">
                        <p>It's svod page</p>
                    </Route>
                    <Route path="/jazz">
                        <p>It's Jazz FAST page</p>
                    </Route>
                    <Route path="/mix">
                        <p>It's Mix FAST page</p>
                    </Route>
                    <Route path="/classic">
                        <p>It's Classic FAST page</p>
                    </Route>
                </Switch>
            </main>
            <footer>

            </footer>
        </Router>
    );
}

export default App;
