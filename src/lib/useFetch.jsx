import {useState, useEffect} from "react";

const useFetch = (url) => {
    const [status, setStatus] = useState("idle");
    const [data, setData] = useState([]);

    useEffect(() => {
        if (!url) return;

        const fetchData = async () => {
            setStatus('fetching');
            await fetch(
                `${url}`,
            {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json;charset=utf-8",
                        "Accept"      : "application/json"
                    }
                }
            ).then(response => response.json())
            .then( json => setData(json))
            .catch( err => console.log(err));
            setStatus('fetched');
        };

        fetchData()
    }, [url]);

    return { status, data };
};

export default useFetch;