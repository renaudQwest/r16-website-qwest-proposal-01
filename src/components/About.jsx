import React from 'react'
import useFetch from "../lib/useFetch";

const About = () => {

    const { status, data } = useFetch("http://localhost:3000/about");

    console.log("ABOUT", data);

    const insertText = (paragraph) => {
        return {
            __html: paragraph
        }
    }

    return (
        <section className="mt-5">
            <div className="container">
                {
                    status === "fetched" && data && (
                        <React.Fragment>
                        <div className="row">
                            <section id="intro" className="col-12 col-md-4">
                                <h1>{data.greatTitle}</h1>
                                <p dangerouslySetInnerHTML={insertText(data.introduction)} />
                            </section>
                            <section className="col-12 col-md-8">
                                {
                                    data.titles.map( (title, index) => (
                                        <React.Fragment>
                                            <h2 style={{ "padding": "0.125rem 0 1rem 0" }}>{title}</h2>
                                            <p>{data.paragraph[index]}</p>
                                        </React.Fragment>
                                    ))
                                }
                            </section>
                        </div>
                        </React.Fragment>
                    )
                }
            </div>
        </section>
    )
}

export default About;