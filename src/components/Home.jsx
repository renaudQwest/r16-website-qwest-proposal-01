import React from 'react'
import Announcements from "./Home/announcement";
import News from "./Home/News";
import OnAir from "./Home/OnAir";
import Promo from "./Home/Promotion";

const Home = () => {

    return (
        <section>
            <div className="container">
                <Announcements />
            </div>
            <div className="container">
                <OnAir />
            </div>
            <Promo />
            <div className="container">
                <News />
            </div>
        </section>
    );
}
export default Home;