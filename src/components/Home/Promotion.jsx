import React from "react";
import useFetch from "../../lib/useFetch";
import "./promotion.scss";
import {Snowfall, Snowflake} from 'react-snowflakes';

const Promo = () => {

    const { status, data } = useFetch("http://localhost:3000/promo");

    const {title, catchPhrase, price} = data;

    return(
        <React.Fragment>
        {
            status === "fetched" && data && (
                <section className="promotion-xmass mt-5 pt-5">
                    <Snowfall count={50}
                              style={{
                                  position: 'relative',
                                  width: '100%',
                                  height: '60vh'
                              }}
                              snowflakeFactory={index => {
                                  const size = index / 50;
                                  const w = 5 + 10 * size + 'px';
                                  return (
                                      <Snowflake speed={.05 + size * 2}
                                                 xSpeedPrc={.3 * size}
                                                 ySpeedPrc={.1 * size}
                                                 style={{
                                                     width: w,
                                                     height: w,
                                                     borderRadius: '50%',
                                                     backgroundColor: 'white',
                                                     opacity: .2 + .8 * size,
                                                     filter: `blur(${Math.round(Math.max(size - .5, 0) * 15)}px)`
                                                 }}/>
                                  )
                              }}>

                    </Snowfall>
                    <section className="container promotion-wrapper text-white py-5">
                        <div className="row">
                            <div className="col-12 col-md-4">
                                <h1>{title}</h1>
                                <p>{catchPhrase}</p>
                                <p className="price">
                                    <span className="only">Only</span>
                                    <b>${ price?.us?.toString().split('.')[0]}
                                        <sup className="cent">{"." + Number(price?.us?.toString().split('.')[1])}</sup>
                                    </b>
                                    <div className="barred-price" title="instead of">
                                        <span className="line" />
                                        ${ Number(price?.us?.toString().split(".")[0]) + 3 }
                                        <sup className="cent">{"." + Number(price?.us?.toString().split(".")[1])}</sup>
                                    </div>
                                </p>
                                <button className="btn w-100 btn-danger">I profit now</button>
                            </div>
                            <div className="col-12 col-md-8">
                                <ul className="promotion-devices">
                                    <li className="promotion-mobile">

                                    </li>
                                    <li className="promotion-pad">

                                    </li>
                                    <li className="promotion-desktop">

                                    </li>
                                    <li className="promotion-tv">

                                    </li>
                                </ul>
                            </div>
                        </div>
                    </section>
                </section>
            )
        }
        </React.Fragment>
    )
}

export default Promo;