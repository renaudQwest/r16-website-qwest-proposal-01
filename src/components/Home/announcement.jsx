import React from 'react'
import useFetch from "../../lib/useFetch";
import "./announcement.scss";

const Announcements = () => {
    const {status, data} = useFetch("http://localhost:3000/announcement");

    console.log("ANNOUNCEMENT DATA:", data);

    return (
        <section id="announcements" className="row card-parent">
            {
                data && status === "fetched" && data.map((x, index) => (
                    <div key={index} className="col-lg-4 col-md-12 mb-3 mb-lg-0">
                        <div
                             className={`card-announcement ${(x.type) ? " bg-" + x.type : ""}`}
                             style={{"backgroundImage": x.image}}
                        >
                            <div className="card-title">
                                <h2>{x.title}</h2>
                            </div>
                        </div>
                    </div>
                ))
            }
        </section>
    )
}

export default Announcements;