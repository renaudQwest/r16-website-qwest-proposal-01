import React, {useState} from 'react'
import useFetch from "../../lib/useFetch";
import "./OnAir.scss";

const OnAir = () => {

    const {status, data} = useFetch("http://localhost:3000/channels");
    const {onAir, setOnAir} = useState(data[0]);

    console.log("ON AIR: ", status, data);

    const channelHandler = (channel) => {
        console.log(channel);
    };

    return (
        <section className="mt-5">
            <h1>On Air</h1>
            <section className="row">
                <div className="col-12 col-lg-2">
                    <div className="row">
                        {
                            status === "fetched" && data.map((channel, index) =>
                                (<div key={index} className="col-4 col-lg-12"
                                      style={{"height": "50px"}}
                                      onClick={() => channelHandler(channel)}>
                                    <img src={channel.img?.logo} alt={`Logo with ${channel.name}`}/>
                                </div>)
                            )
                        }
                    </div>
                </div>

                <div className="col-12 col-lg-10">
                    <video className="video-player" controls>
                        <source src="https://player.vimeo.com/external/250688977.sd.mp4?s=d14b1f1a971dde13c79d6e436b88a6a928dfe26b&profile_id=165"/>
                    </video>
                </div>
            </section>
        </section>
    )
}

export default OnAir;