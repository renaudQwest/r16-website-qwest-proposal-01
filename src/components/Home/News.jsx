import React, {useEffect, useState} from "react";
import useFetch from "../../lib/useFetch";
import "./news.scss";

const News = () => {
    const {status, data} = useFetch("http://localhost:3000/news");
    const [placement, setPlacement] = useState("carousel");

    console.log("NEWS DATA", data);

    return (
        <section className="my-5 row">
            <div className="col-12">
                <section className="d-flex justify-content-between flex-md-nowrap flex-wrap mb-4">
                    <h1>Fresh Contents</h1>
                    <div className="list-group list-group-horizontal border-0">
                        <button
                            className={`list-group-item list-group-item-action border-0 option ${(placement === "carousel") ? "active" : ""}`}
                            onClick={() => setPlacement("carousel")}>Carousel
                        </button>
                        <button
                            className={`list-group-item list-group-item-action border-0 option ${(placement === "doubled") ? "active" : ""}`}
                            onClick={() => setPlacement("doubled")}>Doubled
                        </button>
                        <button
                            className={`list-group-item list-group-item-action border-0 option ${(placement === "grid") ? "active" : ""}`}
                            onClick={() => setPlacement("grid")}>Grid
                        </button>
                        <button
                            className={`list-group-item list-group-item-action border-0 option ${(placement === "list") ? "active" : ""}`}
                            onClick={() => setPlacement("list")}>List
                        </button>
                    </div>
                </section>
            </div>
            {
                placement && placement === "carousel" && (
                    <React.Fragment>
                        <div className="col-12 col-md-6">

                            <div id="carouselExampleFade"
                                 className="carousel slide carousel-fade bg-dark"
                                 data-bs-ride="carousel"
                                 style={{"height": "525px"}}>
                                <ol className="carousel-indicators">
                                    {
                                        data && status === "fetched" && data?.carousel?.map((x, index) => (
                                            <li key={index} data-target="#carouselExampleIndicators"
                                                data-slide-to={index}
                                                className={`${(index === 0) ? "active" : ""}`}></li>
                                        ))
                                    }
                                </ol>
                                <div className="carousel-inner">
                                    {
                                        data && status === "fetched" && data?.carousel?.map((x, index) => (
                                            <div key={index}
                                                 className={`carousel-item ${(index === 0) ? "active" : ""}`}>
                                                <h2>{x.title}</h2>
                                                <img src={x.img} className="d-block w-100" alt="..."/>
                                            </div>
                                        ))
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-md-6 mt-4 mt-md-0">
                            {
                                data && status === "fetched" && data?.fixed?.map((x, index) => (
                                    <article key={index} className="bg-dark mb-4"
                                         style={{"backgroundImage": x.img, "height": ((525 / 3) - 16) + "px"}}>
                                        <h2>{x.title}</h2>
                                    </article>
                                ))
                            }
                        </div>
                    </React.Fragment>
                )
            }
            {
                placement && placement === "doubled" && (
                    <React.Fragment>
                        <div className="col-12 col-md-4 mt-4 mt-md-0">
                            {
                                data && status === "fetched" && data?.fixed?.map((x, index) => (
                                    <article key={index} className="bg-dark mb-4"
                                             style={{"backgroundImage": x.img, "height": ((525 / 3) - 16) + "px"}}>
                                        <h2>{x.title}</h2>
                                    </article>
                                ))
                            }
                        </div>
                        <div className="col-12 col-md-4">

                            <div id="carouselExampleFade"
                                 className="carousel slide carousel-fade bg-dark"
                                 data-bs-ride="carousel"
                                 style={{"height": "525px"}}>
                                <ol className="carousel-indicators">
                                    {
                                        data && status === "fetched" && data?.carousel?.map((x, index) => (
                                            <li key={index} data-target="#carouselExampleIndicators"
                                                data-slide-to={index}
                                                className={`${(index === 0) ? "active" : ""}`}></li>
                                        ))
                                    }
                                </ol>
                                <div className="carousel-inner">
                                    {
                                        data && status === "fetched" && data?.carousel?.map((x, index) => (
                                            <div key={index}
                                                 className={`carousel-item ${(index === 0) ? "active" : ""}`}>
                                                <h2>{x.title}</h2>
                                                <img src={x.img} className="d-block w-100" alt="..."/>
                                            </div>
                                        ))
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-md-4 mt-4 mt-md-0">
                            {
                                data && status === "fetched" && data?.fixed?.map((x, index) => (
                                    <article key={index} className="bg-dark mb-4"
                                         style={{"backgroundImage": x.img, "height": ((525 / 3) - 16) + "px"}}>
                                        <h2>{x.title}</h2>
                                    </article>
                                ))
                            }
                        </div>
                    </React.Fragment>
                )
            }
            {
                placement && placement === "grid" && (
                    <React.Fragment>
                        <div className="col-12">
                            <section className="row grid-main mb-4">
                                {
                                    data && status === "fetched" && data?.carousel?.map((x, index) =>
                                        (
                                            <div key={index} className="col-12 col-md-4 mb-5 mb-md-0">
                                                 <article className="bg-dark item" style={{"backgroundImage": x.image}}>
                                                     <h2>{x.title}</h2>
                                                 </article>
                                            </div>
                                        )
                                    )
                                }
                            </section>
                            <section className="row grid-secondary">
                                {
                                    data && status === "fetched" && data?.fixed?.map((x, index) => (
                                        <div key={index} className="col-12 col-md-4 mb-5 mb-md-0">
                                            <article className="bg-dark item" style={{"backgroundImage": x.image}}>
                                                <h2>{x.title}</h2>
                                            </article>
                                        </div>
                                    ))
                                }
                            </section>
                        </div>
                    </React.Fragment>
                )
            }
            {
                placement && placement === "list" && (
                    <React.Fragment>
                        {
                            data && status === "fetched" && data?.carousel?.map((x, index) => (
                              <article key={index} className="col-12 bg-dark item mb-4" style={{"backgroundImage": x.image}}>
                                  <h2>{x.title}</h2>
                              </article>
                            ))
                        }
                        {
                            data && status === "fetched" && data?.carousel?.map((x, index) => (
                                <article key={index} className="col-12 bg-dark item mb-4" style={{"backgroundImage": x.image}}>
                                    <h2>{x.title}</h2>
                                </article>
                            ))
                        }
                    </React.Fragment>
                )
            }
        </section>
    )
}

export default News;